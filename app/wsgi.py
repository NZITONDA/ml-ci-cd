from main import app

# run app in non blocking mode using waitless

# waitress-serve --listen=127.0.0.1:5000 wsgi:app

# run app in non blocking mode using gunicorn
# gunicorn -b localhost:5000 -w 4 wsgi:app