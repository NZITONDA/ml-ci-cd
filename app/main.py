from flask import Flask,request, jsonify
import pandas as pd
import joblib
import numpy as np


app = Flask(__name__)
# Load sklearn model
model = joblib.load("../model/best_model.joblib")

@app.route("/",methods=["POST"])
def predict():

    # get input data
    data = request.json
    loan_id = data["Loan_ID"]
    del data["Loan_ID"]

    # change json data to dataframe
    data_dict = {k:[data[k] if data[k] !="" else np.nan ] for k in data}
    df = pd.DataFrame(data_dict)

    # run prediction
    labels = model.classes_
    results = model.predict_proba(df)
    predIndex = np.argmax(results)
    pred_class = labels[predIndex]
    probs = dict(zip(labels, results[0]))

    # change format response
    response = {
        "Loan_ID":loan_id,
        "prediction": pred_class,
        "prediction_probabilities": probs
    }
    # send the response
    return jsonify(response),200

if __name__ == "__main__":
    app.run()