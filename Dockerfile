FROM python:3.7-slim
WORKDIR /usr/home/
COPY requirement.txt .
RUN pip install --no-cache-dir -r requirement.txt
RUN mkdir app
RUN mkdir model
COPY app/*  app/
COPY model/*  model/
# ENV PORT=5000
EXPOSE $PORT
WORKDIR /usr/home/app
CMD gunicorn -b 0.0.0.0:$PORT wsgi:app

